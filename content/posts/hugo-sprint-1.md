---
title: "Hugo Sprint-1"
date: 2021-04-02T19:40:22+05:30
draft: false
---
Step 4: Add Some Content

You can manually create content files (for example as content/<CATEGORY>/<FILE>.<FORMAT>) and provide metadata in them, however you can use the new command to do a few things for you (like add title and date):

hugo new posts/my-first-post.md
